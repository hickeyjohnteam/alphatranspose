program ScalapackTest
  use iso_fortran_env
implicit none
integer:: mpiSize, mpiRank
integer:: nRow, nCol, con, myRow, myCol
integer(kind=8)::IAM, nProcs
integer:: i,j
integer:: N, kr, kc, rowSrc, colSrc, kc2
real(real64), dimension(:,:), allocatable:: localArray, localArray2
real(real64), dimension(16,16):: globalArray
integer:: blockColSize, blockRowSize, numRowBlocks, numColBlocks
integer:: fileUnit, numRow, numCol, l, l2, ierr
integer:: startLocalRow, startCol, startTempRow, startTempCol, startLocalCol
real(real64), dimension(:, :), allocatable:: tempArray
integer, dimension(9):: DES, DES2

!if (mpiRank==0) then
!  do i = 1, 16
!    do j = 1, 16
!      if (i==j) then
!      GlobalArray(i,j) = 2!16*(i-1) +j
!    else 
!      GlobalArray(i,j) =0
!    endif 
!    end do
!  end do
!else
!  GlobalArray = mpiRank
!end if

!if (mpiRank==0) then
!  open(newunit=fileUnit, file="matrix", action="write", status="replace")
!  do i = 1, 16
!    write(fileUnit, "(16f10.4)") GlobalArray(i, :)
!  end do
!end if

call BLACS_PINFO(mpiRank, nProcs)

call setUpGrid(con, nRow, nCol, myRow, myCol)

call getMatrixSize("matrix", numRow, numCol)


!rowSrc = myRow
!colSrc = myCOl


call getBlockSize(nRow, nCol, numRow, numCol, blockColSize, blockRowSize)
!blockRowSize = 2
!blockColSize = 2
!
numRowBlocks = numRow/blockRowSize
numColBlocks = numCol/blockColSize

if (numColBlocks*blockColSize .ne. numCol) then
  write(*, "()") "We have a problem allocating the blocksize for the columns"
  stop
end if

if (numRowBlocks*blockRowSize .ne. numRow) then
  write(*, "()") "We have a problem allocating the blocksize for the rows"
  stop
end if

write(*,*) "NUMBLOCKS: ", numRowBlocks, numColBlocks
write(*,*) "BLOCKSIZE: ", blockRowSize, blockColSize
write(*,*) "MATRIXSIZE:", numRow, numCol

call DESCINIT(DES, numRow, numCol, blockRowSize, blockColSize, 0, 0, con, blockRowSize*numRowBlocks/nRow, ierr)
call DESCINIT(DES2, numRow, numCol, blockRowSize, blockColSize, 0, 0, con, blockRowSize*numRowBlocks/nRow, ierr)

l=1
allocate(tempArray(blockRowSize*nRow,numCol))
allocate(localArray(blockRowSize*numRowBlocks/nRow, blockColSize*numColBlocks/nCol))
allocate(localArray2(blockRowSize*numRowBlocks/nRow, blockColSize*numColBlocks/nCol))
write(*,*) "LOCAL ARRAY: ", size(localArray, 1), size(localArray, 2)

localArray = -2
open(newunit=fileUnit, file="matrix", action="read")

tempArray = -10

do i = 0, numRow/(blockRowSize*nRow)-1
  do j = 1, blockRowSize*nRow
    read(fileUnit, *) tempArray(j,:)
  end do
!  if (mpiRank==0) then
!  write(*,*) "------------------"
!  do j = 1, blockRowSize*nRow
!    write(*,*) tempArray(j,: )
!  end do
!  write(*,*) "------------------"
!end if

  startLocalRow = (i)*blockRowSize+1

  startTempRow = myRow*blockRowSize

  do j = 0, numCol/(nCol*blockColSize)-1
    startTempCol = myCol*blockColSize+j*blockColSize*nCol+1
    startLocalCOl = (j)*blockColSize+1

    localArray(startLocalRow: startLocalRow+blockRowSize-1,startLocalCol:startLocalCol+blockColSize-1) = tempArray( startTempRow+1:startTempRow+blockRowSize,startTempCol: startTEmpCol+blockColSize-1)
!!    if (mpiRank==0) then
!      write(*,*) "--------------------------------------"
!      write(*,*) "ROWBLOCK", i, "COLBLOCK", j, numCol/(nCol*blockColSize)
!      write(*,"(8(I0,x))") startLocalRow, startLocalRow+blockRowSize-1, startLocalCol, startLocalCol+blockColSize-1, startTempRow+1, startTempRow+blockRowSize, startTempCol, startTempCol+blockColSize-1
!      write(*,*) localArray(1:startLocalRow+blockRowSize-1, startCol:startCol+blockCOlSize-1)
!      write(*,*)
!      write(*,*) tempArray(startTempRow+1: startTempRow+blockRowSize,startCol:startCol+blockColSize-1)
!      write(*,*) 
!      write(*,*) "--------------------------------------"
!    end if

  end do
end do
write(*,*) "ARRGH", blockRowSize*numRowBlocks/nRow, blockColSize*numColBlocks/nCol, myRow*blockRowSize+1, myCol*blockColSize+1

call PDGEMM('N', 'T', numRow,numRow,numCol, 1.0_real64, localArray, 1,1, DES, localArray, 1,1, DES, 0.0_real64, localArray2, 1,1, DES2)

!call PDSYEV('V', 'U', numRow*numCol/(nRow*nCol), localArray2, 1,1, DES2, 

if (mpiRank==0) then
  do i = 1, size(localArray2(:,1))
  write(*,"(16f10.1)") localArray2(i,:)
end do
end if
!do i = 1,  8
!  write(100+mpiRank,"(16f10.4)") localArray(i,:)
!end do



!if (mpiRank==0) then
!  write(*,*) rowSrc, nRow, blockRowSize, blockColSize
!end if
!do i = 0, numRowBlocks/nRow-1
!  kr = rowSrc+nRow*blockRowSize*i
!  do j = 0, numColBlocks/nCol-1
!!  do j = 1, N
!    kc = colSrc+nCol*blockColSize*j
!    if (mpiRank==1) then
!      write(*,*) kr, kr+blockRowSize, kc, kc+BlockColSize
!      write(*,"(3e15.6)") globalArray(kr:kr+blockRowSize-1, kc:kc+blockColSize-1)
!  end if
!  end do
!end do
    

contains
  !> @brief Get the size of an external matrix
  !> @details Gets the size of a matrix that is stored on disk. It is assumed
  !> that there is a line per row, and the the matrix is seperated by either
  !> tabs or spaces
  !> @author Diarmaid de Búrca
  subroutine getMatrixSize(fileNameIn, numRowsOut, numColsOut)
    use AlphaHouseMod, only: countLines, countColumns
    character(len=*), intent(in):: fileNameIn
    integer, intent(out):: numRowsOut, numColsOut

    numRowsOut = countLines(fileNameIn)
    numColsOut = countColumns(fileNameIn, [" ", char(9)])

  end subroutine getMatrixSize

  !> @brief Sets up a grid of processors to use with scalapack
  !> @details The processor grid required by scalapack has to be rectangular.
  !> This will set up the grid by deviding the total number of processors by each
  !> number in sequence until the remainder is zero.   It then sets the number
  !> of rows to be the the number reached, and the number of columns to be the
  !> total number of processors divided by the number of processors in a row.
  !> Note that we are guareented that the number of columns will be a real number
  !> (mod is zero).
  !> @author Diarmaid de Búrca
  
  subroutine setUpGrid(context, numRowProc, numColProc, myRow, myCol)
    integer, intent(out):: context
    integer, intent(out):: numRowProc, numColProc, myRow, myCol
    integer:: mpiRank, nProcs

    call BLACS_PINFO(mpiRank, nProcs)

    outLoop: do i = 2, nProcs
      if (mod(nProcs,i)==0) then
        numRowProc = i
        numColProc = nProcs/i
        exit outLoop
      end if
    end do outLoop

    call SL_INIT(context, numRowProc, numColProc)
    
    call BLACS_GRIDINFO(context, numRowProc, numColProc, myRow, myCol)

  end subroutine setUpGrid

  !> @brief Get the size of each mini block
  !> @details I'm not 100% sure this works, but the idea is that it has a
  !> minimum size of 1, a maximum block size of 64 and should move smoothly
  !> between them. Untested - completely untested!
  !> @author Diarmaid de Búrca
  subroutine getBlockSize(rProc, cProc, mRowSize, mColSize, blockColOut, blockRowOut)
    integer, intent(in):: rProc, cProc
    integer, intent(in):: mRowSize, mColSize
    integer, intent(out):: blockColOut, blockRowOut

    blockRowOut = mRowSize/(4*rProc)
    if (blockRowOut>64) then
      blockRowOut = 64
    else if (blockRowOut==0) then
      blockRowOut = 1
    end if

    blockColOut = mColSize/(4*cProc)
    if (blockColOut >64) then
      blockColOut = 64
    else if (blockColOut == 0) then
      blockColOut = 1
    end if
  end subroutine getBlockSize

  subroutine testScalapack

  end subroutine testScalapack
end program ScalapackTest
